# base project

- cd frontend
- ng build --prod
- docker-compose up --build

## frontend
http://localhost:8081/

## backend
http://localhost:10000/WeatherForecast
